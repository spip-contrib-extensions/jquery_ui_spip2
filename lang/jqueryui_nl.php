<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/jqueryui?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_calendrier' => 'Toon de kalender',

	// C
	'cfg_boite_jqueryui' => 'Configuratie van jQuery UI',
	'cfg_explication_plugins' => 'Kies hier de in te voegen plugins voor de publieke site.',
	'cfg_explication_themes' => 'Kies de thema`s voor de jQuery-UI interface.',
	'cfg_lbl_plugins' => 'Plugins',
	'cfg_lbl_themes' => 'Thema',
	'cfg_no_css' => 'laad de CSS van jQuery-UI niet',
	'cfg_titre_jqueryui' => 'jQuery UI',
	'cfg_val_complete' => 'De complete jQuery UI',
	'cfg_val_effects_blind' => 'Blind effect',
	'cfg_val_effects_bounce' => 'Bounce effect',
	'cfg_val_effects_clip' => 'Clip effect',
	'cfg_val_effects_core' => 'Core effecten',
	'cfg_val_effects_drop' => 'Drop effect',
	'cfg_val_effects_explode' => 'Explode effect',
	'cfg_val_effects_fold' => 'Fold effect',
	'cfg_val_effects_highlight' => 'Highlight',
	'cfg_val_effects_pulsate' => 'Pulsate effect',
	'cfg_val_effects_scale' => 'Scale effect',
	'cfg_val_effects_shake' => 'Shake effect',
	'cfg_val_effects_slide' => 'Slide effect',
	'cfg_val_effects_transfer' => 'Transfer effect',
	'cfg_val_ui_accordion' => 'UI accordion',
	'cfg_val_ui_autocomplete' => 'UI autocomplete',
	'cfg_val_ui_button' => 'UI button',
	'cfg_val_ui_core' => 'Core UI',
	'cfg_val_ui_datepicker' => 'UI datepicker',
	'cfg_val_ui_dialog' => 'UI dialog',
	'cfg_val_ui_draggable' => 'UI draggable',
	'cfg_val_ui_droppable' => 'UI droppable',
	'cfg_val_ui_mouse' => 'UI mouse',
	'cfg_val_ui_position' => 'UI position',
	'cfg_val_ui_progressbar' => 'UI progressbar',
	'cfg_val_ui_resizable' => 'UI resizable',
	'cfg_val_ui_selectable' => 'UI selectable',
	'cfg_val_ui_slider' => 'UI slider',
	'cfg_val_ui_sortable' => 'UI sortable',
	'cfg_val_ui_tabs' => 'UI tabs',
	'cfg_val_ui_widget' => 'UI widget',

	// D
	'date_mois_10_abbr' => 'okt',
	'date_mois_11_abbr' => 'nov',
	'date_mois_12_abbr' => 'dec',
	'date_mois_1_abbr' => 'jan',
	'date_mois_2_abbr' => 'feb',
	'date_mois_3_abbr' => 'maa',
	'date_mois_4_abbr' => 'apr',
	'date_mois_5_abbr' => 'mei',
	'date_mois_6_abbr' => 'jun',
	'date_mois_7_abbr' => 'jul',
	'date_mois_8_abbr' => 'aug',
	'date_mois_9_abbr' => 'sep'
);
