<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/jqueryui?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'afficher_calendrier' => 'Mostrar el calendario',

	// C
	'cfg_boite_jqueryui' => 'Configuración de jQuery UI',
	'cfg_explication_plugins' => 'Elija los plugins a insertar en el encabezado de las páginas públicas.',
	'cfg_explication_themes' => 'Elija el tema gráfico para jQuery-UI',
	'cfg_lbl_plugins' => 'Plugins',
	'cfg_lbl_themes' => 'Tema',
	'cfg_no_css' => 'No cargar los CSS de jQuery-UI',
	'cfg_titre_jqueryui' => 'jQuery UI',
	'cfg_val_complete' => 'jQuery UI completo',
	'cfg_val_effects_blind' => 'Efecto blind',
	'cfg_val_effects_bounce' => 'Efecto bounce',
	'cfg_val_effects_clip' => 'Efecto clip',
	'cfg_val_effects_core' => 'Efectos Core',
	'cfg_val_effects_drop' => 'Efecto drop',
	'cfg_val_effects_explode' => 'Efecto explode',
	'cfg_val_effects_fold' => 'Efecto fold',
	'cfg_val_effects_highlight' => 'Efecto highlight',
	'cfg_val_effects_pulsate' => 'Efecto pulsate',
	'cfg_val_effects_scale' => 'Efecto scale',
	'cfg_val_effects_shake' => 'Efecto shake',
	'cfg_val_effects_slide' => 'Efecto slide',
	'cfg_val_effects_transfer' => 'Efecto transfer',
	'cfg_val_ui_accordion' => 'UI accordion',
	'cfg_val_ui_autocomplete' => 'UI autocomplete',
	'cfg_val_ui_button' => 'UI button',
	'cfg_val_ui_core' => 'Core UI',
	'cfg_val_ui_datepicker' => 'UI datepicker',
	'cfg_val_ui_dialog' => 'UI dialog',
	'cfg_val_ui_draggable' => 'UI draggable',
	'cfg_val_ui_droppable' => 'UI droppable',
	'cfg_val_ui_mouse' => 'UI mouse',
	'cfg_val_ui_position' => 'UI position',
	'cfg_val_ui_progressbar' => 'UI progressbar',
	'cfg_val_ui_resizable' => 'UI resizable',
	'cfg_val_ui_selectable' => 'UI selectable',
	'cfg_val_ui_slider' => 'UI slider',
	'cfg_val_ui_sortable' => 'UI sortable',
	'cfg_val_ui_tabs' => 'UI tabs',
	'cfg_val_ui_widget' => 'UI widget',

	// D
	'date_mois_10_abbr' => 'oct.',
	'date_mois_11_abbr' => 'nov.',
	'date_mois_12_abbr' => 'dic.',
	'date_mois_1_abbr' => 'ene.',
	'date_mois_2_abbr' => 'feb.',
	'date_mois_3_abbr' => 'mar.',
	'date_mois_4_abbr' => 'abr.',
	'date_mois_5_abbr' => 'mayo',
	'date_mois_6_abbr' => 'jun.',
	'date_mois_7_abbr' => 'jul.',
	'date_mois_8_abbr' => 'ago.',
	'date_mois_9_abbr' => 'sept.'
);
